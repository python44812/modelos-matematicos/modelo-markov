import tkinter as tk
from tkinter import ttk
import numpy as np

class MarkovApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Modelo de Márkov")

        # Crear la sección para el vector de estado inicial (X0)
        ttk.Label(root, text="Vector de estado inicial (X0):").grid(row=0, column=0, columnspan=3, sticky="w")
        self.x0_entries = []
        for i in range(3):
            entry = ttk.Entry(root, width=10)
            entry.grid(row=1, column=i, padx=5, pady=5)
            self.x0_entries.append(entry)

        # Crear la sección para la matriz de probabilidad (P)
        ttk.Label(root, text="Matriz de probabilidad (P):").grid(row=2, column=0, columnspan=3, sticky="w")
        self.p_entries = []
        for i in range(3):
            row_entries = []
            for j in range(3):
                entry = ttk.Entry(root, width=10)
                entry.grid(row=3 + i, column=j, padx=5, pady=5)
                row_entries.append(entry)
            self.p_entries.append(row_entries)

        # Etiquetas y entradas para el número de periodos
        ttk.Label(root, text="Número de periodos:").grid(row=6, column=0, sticky="w")
        self.periodos_entry = ttk.Entry(root, width=10)
        self.periodos_entry.grid(row=6, column=1, sticky="w")

        # Botón para calcular
        self.calcular_button = ttk.Button(root, text="Calcular", command=self.calcular)
        self.calcular_button.grid(row=7, column=0, columnspan=3)

        # Text widget para mostrar el resultado
        self.resultado_text = tk.Text(root, height=20, width=50)
        self.resultado_text.grid(row=8, column=0, columnspan=3, pady=10)

    def calcular(self):
        try:
            # Leer el vector de estado inicial
            x0 = [float(entry.get()) for entry in self.x0_entries]

            # Leer la matriz de probabilidad
            p = []
            for row_entries in self.p_entries:
                row = [float(entry.get()) for entry in row_entries]
                p.append(row)

            # Convertir a arrays de numpy
            x0 = np.array(x0)
            p = np.array(p)

            # Leer el número de periodos
            periodos = int(self.periodos_entry.get())

            # Limpiar el texto del resultado
            self.resultado_text.delete(1.0, tk.END)

            # Mostrar el vector de estado inicial
            self.resultado_text.insert(tk.END, "X0: {}\n".format(x0))

            # Calcular el estado para cada periodo
            x = x0
            for i in range(1, periodos + 1):
                x = np.dot(x, p)
                self.resultado_text.insert(tk.END, "X{}: {}\n".format(i, np.around(x, decimals=2)))

        except Exception as e:
            self.resultado_text.insert(tk.END, "Error: {}\n".format(e))

if __name__ == "__main__":
    root = tk.Tk()
    app = MarkovApp(root)
    root.mainloop()
